package se.vpklotar.towerdefence;

import java.util.ArrayList;

import com.badlogic.gdx.InputProcessor;

public class EventManager implements InputProcessor {
	public static EventManager INSTANCE;
	public static int diffTolerans = 5;
	public ArrayList<Input> LISTENERS = new ArrayList<Input>();
	
	
	public EventManager() {
		INSTANCE = this;
	}
	
	public int InvertY(int value) {
		return (int)MainClass.HEIGHT - value;
	}
	
	public boolean Within(int val, int sourceVal, int diff) {
		return (val-sourceVal) <= diff;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	private int X,Y;
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		screenY = InvertY(screenY);
		X = screenX;
		Y = screenY;
		
		for(int i = 0; i < LISTENERS.size(); i++) {
			LISTENERS.get(i).touchDown(screenX, screenY, pointer, button);
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		screenY = InvertY(screenY);
		
		for(int i = 0; i < LISTENERS.size(); i++) {
			LISTENERS.get(i).touchUp(screenX, screenY, pointer, button);
		}
		
		if(Within(screenX, X, diffTolerans) && Within(screenY, Y, diffTolerans)) {
			click(screenX, screenY);
		}
		
		return false;
	}
	
	public void click(int x, int y) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			Input in = LISTENERS.get(i);
			if(in.Within(x, y)) {
				in.click(x, y);
			}
		}
		
		// Make sure that no click be done without a Down AND Up event 
		X = diffTolerans / -1 - 100;
		Y = diffTolerans / -1 - 100;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
}

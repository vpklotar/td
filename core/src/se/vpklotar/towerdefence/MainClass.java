package se.vpklotar.towerdefence;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import se.vpklotar.towerdefence.scenes.BaseScene;
import se.vpklotar.towerdefence.scenes.InGame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class MainClass extends Game {
	SpriteBatch batch;
	ShapeRenderer shape;
	
	public static BaseScene SCENE;
	public static float WIDTH, HEIGHT;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		shape = new ShapeRenderer();
		
		Gdx.input.setInputProcessor(new EventManager());
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if(SCENE != null) SCENE.Render(batch, shape);
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		WIDTH = (float)width;
		HEIGHT = (float)height;
		
		if(SCENE == null) SetScene(new InGame());
	}
	
	public static void SetScene(BaseScene scene) {
		SCENE = scene;
	}
	
	public static float GetDiff(float val1, float val2) {
		if(val1 > val2) return val1 - val2;
		if(val1 < val2) return val2 - val1;
		return 0F;
	}
	
	public static int GetDiffInt(int val1, int val2) {
		return (int)GetDiff(val1, val2);
	}
	
	public static String ReadStringURL(String url) {
		try{
			URL website = new URL(url);
	        URLConnection connection = website.openConnection();
	        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

	        StringBuilder response = new StringBuilder();
	        String inputLine;

	        while ((inputLine = in.readLine()) != null) 
	            response.append(inputLine);

	        in.close();

	        return response.toString();
		}catch(Exception ex) {
			return "";
		}
	}
}

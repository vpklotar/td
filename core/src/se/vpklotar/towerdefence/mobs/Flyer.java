package se.vpklotar.towerdefence.mobs;

public class Flyer extends BaseMob {

	public Flyer() {
		speed = 2.5f;
		coin = 25;
		health = 10;
		spawn_level = 10;
		Texture = new com.badlogic.gdx.graphics.Texture("Mob-two.png");
	}
	
}

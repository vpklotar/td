package se.vpklotar.towerdefence.mobs;

import se.vpklotar.towerdefence.MainClass;
import se.vpklotar.towerdefence.Point;
import se.vpklotar.towerdefence.world.Block;
import se.vpklotar.towerdefence.world.Unit;
import se.vpklotar.towerdefence.world.World;

public abstract class BaseMob extends Unit {
	public float speed, strength, health, coin, spawn_level;
	private int PointIndex = 0;
	
	public BaseMob() {
		super(0, 0);
	}
	
	@Override
	public void Tick() {
		Move();
	}
	
	public Block OnWayTo() {
		return World.GetBlockWorld((int)GetPoint().X, (int)GetPoint().Y);
	}
	
	public Point GetPoint() {
		return World.GetRoadMap().get(PointIndex);
	}
	
	private void Move() {
		if(PointIndex < World.GetRoadMap().size() && (X == OnWayTo().X && Y == OnWayTo().Y)) {
			PointIndex++;
		}
		
		
		if(PointIndex >= World.GetRoadMap().size()) {
			// TODO: Implement die logic here
			return;
		}
		
		float speed = this.speed;
		
		if(X == OnWayTo().X) {
			if(speed > MainClass.GetDiff(Y, OnWayTo().Y)) speed = MainClass.GetDiff(Y, OnWayTo().Y);
			if(OnWayTo().Y < Y) speed /= -1;
			applyY(speed);
		}else if(Y == OnWayTo().Y) {
			if(speed > MainClass.GetDiff(X, OnWayTo().X)) speed = MainClass.GetDiff(X, OnWayTo().X);
			if(OnWayTo().X < X) speed /= -1;
			applyX(speed);
		}
	}

}

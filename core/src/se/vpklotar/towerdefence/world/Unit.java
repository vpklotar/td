package se.vpklotar.towerdefence.world;

public abstract class Unit extends Block {
	
	public Unit(float x, float y) {
		super(x, y);
	}

	public void Tick() { }
}

package se.vpklotar.towerdefence.world;

import java.util.ArrayList;

import se.vpklotar.towerdefence.MainClass;
import se.vpklotar.towerdefence.Point;
import se.vpklotar.towerdefence.mobs.BaseMob;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

public class World {
	public static int TICKS_PER_SEC = 20;
	public static long LAST_TICK = 0;
	public static float BLOCK_SIZE = 32;
	public static float BLOCKS_WIDE, BLOCKS_HIGH;
	private static ArrayList<Point> roadmap = new ArrayList<Point>();
	public static boolean ReBuildAfterMapPointChange = true; // TODO: Change at release for optimized performance
	
	private static ArrayList<Block> blocks = new ArrayList<Block>();
	private static ArrayList<Unit> tickUnites = new ArrayList<Unit>();
	private Block SpawnBlock;
	
	public void AddBlock(Block block) {
		blocks.add(block);
	}
	
	public void RemoveBlock(Block block) {
		blocks.remove(block);
	}
	
	public ArrayList<Block> GetBlocks() {
		return blocks;
	}

	public void RemoveBlock(float x, float y) {
		for(int i = 0; i < blocks.size(); i++) {
			if(blocks.get(i).getX() == x && blocks.get(i).getY() == y) {
				blocks.remove(i);
				return;
			}
		}
	}
	
	public static Block GetBlock(float x, float y) {
		for(int i = 0; i < blocks.size(); i++) {
			Block b = blocks.get(i);
			if((x >= b.getX() && x <= b.getX()) &&  (y >= b.getY() && y <= b.getY())) {
				return b;
			}
		}
		return null;
	}
	
	public static Block GetBlockWorld(int x, int y) {
		return GetBlock(x * BLOCK_SIZE, y * BLOCK_SIZE);
	}
	
	/**
	 * Use this option to auto-detect the right size to use for covering the screen
	 */
	public World() {
		BLOCKS_WIDE = (int) Math.ceil(MainClass.WIDTH / BLOCK_SIZE);
		BLOCKS_HIGH = (int) Math.ceil(MainClass.HEIGHT / BLOCK_SIZE);
		GenerateWorldBase();
	}
	
	/**
	 * This is a constructor to build a world according to this specifications, roadmaps will be applied later.
	 * @param width Number of blocks to be wide
	 * @param height Number of blocks to be high
	 */
	public World(float width, float height) {
		BLOCKS_WIDE = width;
		BLOCKS_HIGH = height;
		GenerateWorldBase();
	}
	
	private void GenerateWorldBase() {
		Texture tex = new Texture("bg.png");
		System.out.println("[INFO] Generation world background (" + BLOCKS_WIDE + "b, " + BLOCKS_HIGH + "b)");
		for(int x = 0; x < BLOCKS_WIDE; x++) {
			for(int y = 0; y < BLOCKS_HIGH; y++) {
				Block b = new Block(x * BLOCK_SIZE, y * BLOCK_SIZE);
				b.Texture = tex;
				AddBlock(b);
			}
		}
	}
	
	/**
	 * Loads a map from the main server with the specified name
	 * @param name The name of the map to load from server
	 */
	public void LoadRoadMap(int id) {
		Json json = new Json();
		json.setOutputType(OutputType.json);
		
		String jj = MainClass.ReadStringURL("http://54.89.33.196/GetMap.php?id=" + id);
		JsonValue value = new JsonReader().parse(jj);
		String name = value.getString(0); // Read the name
		String points = value.
		
		Object o = json.fromJson(Object.class, points);
		
		/*if(jj.trim() == "") {
			System.out.println("No json was found in the string");
			return;
		}
		roadmap = (ArrayList<Point>)json.fromJson(ArrayList.class, jj);*/
		
		
		/*AddRoadMapPoint(0, 0);
		AddRoadMapPoint(0, 3);
		AddRoadMapPoint(2, 3);*/
		
		BuildRoad();
	}
	
	private static void BuildRoad() {
		if(roadmap.size() <= 0) AddRoadMapPoint(0, 0);
		
		Point last = roadmap.get(0);
		Point next = new Point(0, 0);
		
		for (int i = 0; i < roadmap.size()-1; i++) {
			Point p = roadmap.get(i);
			next = roadmap.get(i+1);
			
			Block b = GetBlockWorld((int)p.X, (int)p.Y);
			b.Texture = GetTexture(last, p, next);
			
			Texture t = FillRoadBetwen(p, next);
			GetBlockWorld((int)next.X, (int)next.Y).Texture = t;
			last = p;
		}
		
		/*if(roadmap.size() - 1 > 0) {
			Point p = roadmap.get(roadmap.size()-1);
			
			Block bNext = GetBlockWorld((int)p.X, (int)p.Y);
			Block b = GetBlockWorld((int)last.Y, (int)last.Y);
			
			bNext.Texture = b.Texture;
		}*/
	}
	
	/**
	 * Ads a roadmap point and re-calculates and re-makes the path
	 * @param x The x coordinate of the block (not in pixels, but number of blocks)
	 * @param y The y coordinate of the block (not in pixels, but number of blocks)
	 */
	public static void AddRoadMapPoint(int x, int y) {
		roadmap.add(new Point(x, y));
		
		if(ReBuildAfterMapPointChange) BuildRoad();
	}
	
	public static Texture FillRoadBetwen(Point p, Point next) {
		Texture t = new Texture("coin.png");
		if(next.X > p.X) {
			for(int i = (int)p.X+1; i < next.X; i++) {
				Block b = GetBlockWorld(i, (int)p.Y);
				t = new Texture("roadright.png");
				b.Texture = t;
			}
		}else if(next.X < p.X) {
			for(int i = (int)next.X+1; i < p.X; i++) {
				Block b = GetBlockWorld(i, (int)p.Y);
				t = new Texture("roadright.png");
				b.Texture = t;
			}
		}
		
		if(next.Y > p.Y) {
			for(int i = (int)p.Y+1; i < next.Y; i++) {
				Block b = GetBlockWorld((int)p.X, i);
				t = new Texture("roadup.png");
				b.Texture = t;
			}
		}else if(next.Y < p.Y) {
			for(int i = (int)next.Y+1; i < p.Y; i++) {
				Block b = GetBlockWorld((int)p.X, i);
				t = new Texture("roadup.png");
				b.Texture = t;
			}
		}
		return t;
	}
	
	private static Texture GetTexture(Point last, Point current, Point next) {
		if(last.Y == current.Y && current.Y == next.Y){
			return new Texture("roadright.png");
		}

		if(last.X == current.X && current.X == next.X) {
			return new Texture("roadup.png");
		}

		if(last.Y < current.Y && next.X > current.X) {
			return new Texture("turnright.png");
		}

		if(last.Y < current.Y && next.X < current.X) {
			return new Texture("turnleft.png");
		}

		if(last.Y > current.Y && next.X < current.X) {
			return new Texture("turnleftup.png");
		}

		if(last.Y > current.Y && next.X > current.X) {
			return new Texture("turnrightup.png");
		}
		
		if(last.X < current.X && next.Y > current.Y) {
			return new Texture("turnleftup.png");
		}
		if(last.X < current.X && next.Y < current.Y){
			return new Texture("turnleft.png");
		}
		if(last.X < current.X && next.Y < current.Y) {
			return new Texture("turnrightup.png");
		}
		if(last.X > current.X && next.Y < current.Y){
			return new Texture("turnright.png");
		}
		if(last.X > current.X && next.Y > current.Y){
			return new Texture("turnrightup.png");
		}
		
		return new Texture("coin.png");
	}
	
	public void SpawnMob(BaseMob mob) {
		if(SpawnBlock == null) {
			System.out.println("[INFO] There is no spawn block defined. Will spawn at (0,0)");
			SetSpawnBlock(GetBlock(0, 0));
		}
		
		SpawnBlock.Spawn(mob);
	}
	
	public void SetSpawnBlock(Block b) {
		SpawnBlock = b;
	}

	public static void AddTickUnit(Unit unit) {
		tickUnites.add(unit);
	}
	
	public void RemoveTickUnit(Unit unit) {
		tickUnites.remove(unit);
	}
	
	public ArrayList<Unit> GetTickUnits() {
		return tickUnites;
	}

	public static ArrayList<Point> GetRoadMap() {
		return roadmap;
	}
}

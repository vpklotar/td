package se.vpklotar.towerdefence.world;

import se.vpklotar.towerdefence.EventManager;
import se.vpklotar.towerdefence.Input;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Block extends Input {
	public Texture Texture;

	public float getX() {
		return super.X;
	}

	public void setX(float x) {
		X = x;
	}
	
	public void applyX(float x) {
		X += x;
	}

	public float getY() {
		return Y;
	}
	
	public void applyY(float y) {
		Y += y;
	}

	public void setY(float y) {
		Y = y;
	}

	public float getWidth() {
		return Width;
	}

	public void setWidth(float width) {
		Width = width;
	}

	public float getHeight() {
		return Height;
	}

	public void setHeight(float height) {
		Height = height;
	}

	public Block(float x, float y) {
		// Constructor
		setX(x);
		setY(y);
		setWidth(World.BLOCK_SIZE);
		setHeight(World.BLOCK_SIZE);
		EventManager.INSTANCE.LISTENERS.add(this);
	}
	
	public Block(float x, float y, float width, float height) {
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
	}
	
	public void Render(SpriteBatch batch, ShapeRenderer shape) {
		batch.draw(Texture, X, Y, Width, Height);
	}
	
	public void Spawn(Unit unit) {
		World.AddTickUnit(unit);
	}
	
	@Override
	public void click(int x, int y) {
		World.AddRoadMapPoint((int)(x / World.BLOCK_SIZE), (int)(y / World.BLOCK_SIZE));
	}
}

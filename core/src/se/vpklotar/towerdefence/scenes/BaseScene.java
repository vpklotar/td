package se.vpklotar.towerdefence.scenes;

import se.vpklotar.towerdefence.Input;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public abstract class BaseScene extends Input {

	public void Render(SpriteBatch batch, ShapeRenderer shape) { }
	
}

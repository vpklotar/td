package se.vpklotar.towerdefence.scenes;

import java.util.ArrayList;

import se.vpklotar.towerdefence.mobs.Flyer;
import se.vpklotar.towerdefence.world.Block;
import se.vpklotar.towerdefence.world.Unit;
import se.vpklotar.towerdefence.world.World;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class InGame extends BaseScene {
	private World world;
	
	public InGame() {
		world = new World();
		world.LoadRoadMap(3);
		world.SpawnMob(new Flyer());
	}
	
	public void Render(SpriteBatch batch, ShapeRenderer shape) {
		ArrayList<Unit> t = world.GetTickUnits();
		ArrayList<Block> blocks = world.GetBlocks();
		
		if(World.LAST_TICK + ((long)(1000f / (float)World.TICKS_PER_SEC)) <= System.currentTimeMillis()) { // It's time for a new tick
			for(int i = 0; i < t.size(); i++) {
				t.get(i).Tick();
			}
			World.LAST_TICK = System.currentTimeMillis(); // When was the last tick?
		}
		
		batch.begin();
		for(int i = 0;i < blocks.size(); i++) {
			blocks.get(i).Render(batch, shape);
		}
		for(int i = 0; i < t.size(); i++) {
			t.get(i).Render(batch, shape);
		}
		batch.end();
	}
}
